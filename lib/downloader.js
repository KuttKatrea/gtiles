#!/usr/bin/env node

var url = require('url')
var sys = require('util')
var fs = require("fs")
var path = require("path")
var https = require("https")

var utils = require('./utils')

var downloader = module.exports

downloader.execute = function execute() {
  var command = arguments[arguments.length - 1];

  if ( command.zoom === undefined ) {
    sys.print('El párametro zoom es requerido\n')
    process.exit(1)
  }

  function execute_join(downloaded_files) {
    console.log('Uniendo imágenes')

    var xrange = command.xrange.end - command.xrange.begin + 1,
      yrange = command.yrange.end - command.yrange.begin + 1

    var args = ['montage', '-geometry', '256x256', '-tile', xrange + 'x' + yrange]

    for (var y = command.yrange.begin; y <= command.yrange.end; y += 1)
      for (var x = command.xrange.begin; x <= command.xrange.end; x += 1) {
        args.push( downloaded_files[x][y] )
      }

    args.push(command.output + '/joined.png')

    var child_process = require('child_process')

    console.log('gm ' + args.join(' '))

    var child = child_process.spawn('gm', args)

    function onStdOut(data) {
      console.log('stdout: ' + data);
    }

    child.stdout.on('data', onStdOut);

    function onStdErr(data) {
      console.log('stderr: ' + data);
    }

    child.stderr.on('data', onStdErr);

    child.on('exit', function (code) {
      child.stdout.removeListener('data', onStdOut);
      child.stderr.removeListener('data', onStdErr);
      console.log('child process exited with code ' + code);
    });
  }

  function download(z, x, y, path, callback) {
    var pathFormat = "/vt/?hl=es&x=%d&y=%d&z=%d"
    var pathFormat = '/vt/lyrs=m@196096684,highlight:0x902387dda89a4bd5:0x9d76af04119c3702@1%7Cstyle:maps&hl=es-419&src=app&x=%d&y=%d&z=%d&s=G'

    var outputFile = sys.format("%s/%d-%d.png", path, x, y)
    var requestData = {
      host: "mts1.google.com",
      port: "443",
      path: sys.format(pathFormat, x, y, z),
      headers: {
        //"Cookie": "PREF=ID=a83f0fcffd26bafe:TM=1340510708:LM=1340510708:S=5-HIzsaTR1bB2iKP",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0 FirePHP/0.7.1"
      }
    }

    var request = https.get(requestData, function(response) {
      console.log("Downloading: " + requestData.path);

      response.on("error", function (err) {
        console.log("Error in response: ", err);
        file.end()
        callback(err)
      });

      var file = fs.createWriteStream(outputFile);

      response.pipe(file);

      response.on("end", function() {
        console.log("Saved: %s", outputFile)
        file.end()
        callback(null, x, y, outputFile)
      });
    });

    request.on("error", function(err) {
      console.log("Error:", err);
      callback(err)
    });
  }

  function execute_download() {
    var pending_files = {}
    var downloaded_files = []
    var error = false

    function downloaded(err, x, y, file) {
      if (!err) {
        if (!downloaded_files[x])
          downloaded_files[x] = []

        downloaded_files[x][y] = file

        delete pending_files[x][y]
        if ( utils.isEmptyObject(pending_files[x]) ) {
          delete pending_files[x]
        }

        if ( utils.isEmptyObject(pending_files) ) {
          if (error)
            console.log('Hubo errores al descargar los archivos')
          else
            execute_join(downloaded_files);
        }
      } else
        error = true
    }

    for (var x = command.xrange.begin; x <= command.xrange.end; x+= 1) {
      pending_files[x] = {}
      for (var y = command.yrange.begin; y <= command.yrange.end; y+= 1) {
        pending_files[x][y] = true
        download(command.zoom, x, y, command.output, downloaded)
      }
    }
  }

  function validate() {
    var error = false
    var last = Math.pow(2, command.zoom)

    if ((command.xrange.begin < 0) &&
        (command.xrange.end < command.xrange.begin) &&
        (command.xrange.end >= last)) {
      sys.print(sys.format('El rango de X no debe ser igual o mayor a %d para este nivel de zoom (%d)\n', last, command.zoom))
      error = true;
    }

    if ((command.yrange.begin < 0) &&
      (command.yrange.end < command.yrange.begin) &&
      (command.yrange.end >= last)) {
      sys.print(sys.format('El rango de Y no debe ser igual o mayor a %d para este nivel de zoom (%d)\n', last, command.zoom))
      error = true;
    }

    if (!fs.existsSync(command.output)) {
      try {
        fs.mkdirSync(command.output)
      } catch (e) {
        sys.print(sys.format('Error creando el directorio: %s\n', command.output))
        error = true
      }
    } else {
      var stats = fs.statSync(command.output)

      if (!stats.isDirectory()) {
        sys.print('La ruta indicada no es un directorio válido\n')
        error = true
      } else if (!utils.canWriteSync(command.output, process.getuid(), process.getgid(), stats)) {
        sys.print('La ruta indicada no es writable\n')
        error = true
      }
    }

    if (error) {
      sys.print('Existen errores con los valores de entrada\n')
    } else {
      execute_download()
    }
  }

  function confirmOutput() {
    if ( ! command.output )
      command.prompt('Output path: ', function(value) {
        command.output = value
        validate()
      })
    else
      validate()
  }

  function confirmYRange() {
    if ( ! command.yrange )
      command.prompt('YRange path (min:max): ', function(value) {
        try {
          command.yrange = utils.parseIntRange(value)
        } catch(e) {
          sys.print(e, '\n')
          process.exit(1)
        }
        confirmOutput()
      })
    else
      confirmOutput()
  }

  function confirmXRange() {
    if ( ! command.xrange )
      command.prompt('XRange path (min:max): ', function(value) {
        try {
          command.xrange = utils.parseIntRange(value)
        } catch(e) {
          sys.print(e, '\n')
          process.exit(1)
        }
        confirmYRange()
      })
    else
      confirmYRange()
  }

  function begin() {
    confirmXRange();
  }

  begin();
}

var program = require('commander').
  command('download').
  description('Descarga una serie de tiles y las une en una sola imagen').
  action(downloader.execute).
  option('-z, --zoom [zoom]', 'Zoom del cual se van a obtener los tiles', utils.parseDecimalInt).
  option('-x, --xrange [begin:end]',
    'Rango en X que se obtendrán los tiles', utils.parseIntRange).
  option('-y, --yrange [begin:end]',
    'Rango en Y que se obtendrán los tiles', utils.parseIntRange).
  option('-o, --output [dir]', 'Directorio en el que se guardarán los tiles')
