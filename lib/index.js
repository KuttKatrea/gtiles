#!/usr/bin/env node

var url = require('url')
var sys = require('util')
var fs = require("fs")
var path = require("path")
var https = require("https")
var program = require('commander')

var utils = require('./utils')
var downloader = require('./downloader')
var splitter = require('./splitter')

program.
  command('*').
  description('Muestra la ayuda').
  action(program.help)

program.
  parse(process.argv)

if (program.args.length == 0) {
  program.help();
}
