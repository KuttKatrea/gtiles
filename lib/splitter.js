#!/usr/bin/env node
var fs = require('fs'),
  util = require('util'),
  gm = require('gm'),
  shell = require('shelljs')

var splitter = module.exports

splitter.execute = function execute() {
  var command = arguments[arguments.length - 1];

  if (arguments.length <= 1) {
    console.log('No se indicó un archivo el cual procesar')
    command.help();
  }

  var file = arguments[0]
  var output_dir = (arguments.length <= 2) ? './out' : arguments[1]

  if (!fs.existsSync(file)) {
    console.error(util.format('El archivo %s no existe', file))
    process.exit(-1);
  }

  if (!fs.existsSync(output_dir)) {
    try {
      shell.mkdir('-p', output_dir)
    } catch (err) {
      console.error(util.format('No fue posible crear el directorio %s ', output_dir))
      process.exit(-1);
    }
  }

  dirStat = fs.statSync(output_dir)

  if (!dirStat.isDirectory()) {
    console.error(util.format("La ruta %s no es un directorio", output_dir))
  }

  var image = gm(fs.realpathSync(file))

  image.size(function(err, value){
    if (err) {
      console.error(err);
      process.exit(-1);
    }

    var args = ['convert', fs.realpathSync(file), '-crop', command.width + 'x' + command.height, '+adjoin', output_dir + '/temp-%d.png']

    var child_process = require('child_process')

    console.log('gm ' + args.join(' '))

    var child = child_process.spawn('gm', args)

    function onStdOut(data) {
      console.log('stdout: ' + data);
    }

    child.stdout.on('data', onStdOut);

    function onStdErr(data) {
      console.log('stderr: ' + data);
    }

    child.stderr.on('data', onStdErr);

    child.on('exit', function (code) {
      child.stdout.removeListener('data', onStdOut);
      child.stderr.removeListener('data', onStdErr);
      console.log('child process exited with code ' + code);

      var x_range = value.width / command.width;
      var y_range = value.height / command.height;

      for (var i = 0; i < x_range; i += 1) {
        for (var j = 0; j < y_range; j += 1) {
          var x = command.xoffset + i
          var y = command.yoffset + j

          var target = output_dir + '/' + x + '-' + y + '.png'

          shell.rm('-f', target)

          shell.mv(output_dir + '/temp-'+ (i + j * x_range) +'.png', target)
        }
      }
    });
  });
}

var program = require('commander').
  command('split').
  usage('[options] <input-file> <output-dir>').
  description('Divide una imagen en tiles individuales para su uso en un mapa custom').
  action(splitter.execute).
  option('-x, --xoffset [offset]', 'Offset en X del tile', parseInt, 0).
  option('-y, --yoffset [offset]', 'Offset en Y del tile', parseInt, 0).
  option('-w, --width [ancho]', 'Ancho del tile', parseInt, 256).
  option('-h, --height [alto]', 'Alto del tile', parseInt, 256).
  option('-v, --verbose', 'Nivel de verbosidad', false)
